# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


[
  [ 'ION Orchard', '9.30am', '10.30pm', '40.714728', '-73.998672' ],
  [ 'Great World City', '10am', '10pm', '-47.41234', '50.68409' ],
  [ 'Ngee Ann City', '9.45am', '10.15pm', '-54.47216', '110.10900' ],
].each do |d|
  Store.create(name: d[0], open_time: d[1], close_time: d[2], latitude: d[3], longitude: d[4] )
end

