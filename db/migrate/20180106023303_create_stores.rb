class CreateStores < ActiveRecord::Migration[5.2]
  def change
    create_table :stores do |t|
      t.string :name
      t.string :open_time
      t.string :close_time
      t.string :latitude
      t.string :longitude

      t.timestamps
    end
  end
end
