Rails.application.routes.draw do
  get 'stores/index'
  
  namespace :api, constraints: {format: 'json'} do
    namespace :v1 do 
      get '/stores', :action=>'stores', :controller=> 'api'
      get '/store_map_url/:id', :action=>'store_map_url', :controller=> 'api'
    end
  end
end
