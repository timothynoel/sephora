# README


## Setup

1. Install docker https://docs.docker.com/engine/installation/

2. Run server; Open terminal, cd to source folder, run `docker-compose run web`

3. Run client; Open terminal, cd to source folder, then to `spa/stores`, run
   `docker build -t sephora/spa .` Then run `docker run -p 8080:8080 -e
   HOST=0.0.0.0 sephora/spa`

4. Run `docker-compose run web rails db:setup` in another terminal. It will run the seed file to load
initial data into database.

4. Open `http://localhost:8080` in browser to see the app

## Note
1. Whenever you changed Gemfile or Dockerfile, you need to run `docker-compose build`
2. When running rails command, prepend with 'docker-compose run web' 
3. If you see the error, "A server is already running, Check /myapp/tmp/pids/server.pid" when running the `docker-compose up web`, delete the file at "tmp/pids/server.pid", then run the commnad again.
