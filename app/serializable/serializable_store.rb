#this is the file that does serialzation of json api compatible respond of the jsonapi gem

class SerializableStore < JSONAPI::Serializable::Resource
  type 'stores'
  attribute :name
  attribute :open_time
  attribute :close_time
  attribute :latitude
  attribute :longitude
  attribute :created_at
  attribute :updated_at

  attribute :hour do
    "#{@object.open_time} - #{@object.close_time}"
  end

  attribute :map_url do
    @object.map_url
  end
end
