class Api::V1::ApiController < ApplicationController
  def stores
    render jsonapi: Store.all
  end
end
