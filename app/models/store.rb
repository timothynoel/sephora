class Store < ApplicationRecord

  def map_url
    config = Rails.application.config
    map_params = {
      key: config.google_api_key,
      markers: "color:blue|#{self.name}",
      zoom: 17,
      size: '600x300',
      maptype: 'roadmap'
    }
    config.google_api_base + map_params.to_query
  end

end

#security issue
#config.google_api_key by right should not be calling this way.
#In production, this should be replaced by a proxy api 
#for google map api google_api_key.
#to avoid exposing the API key to the client
